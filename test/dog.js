const assert = require('assert');
const { Dog } = require('../dog');

describe('Dog', function() {
  describe('#bark', function() {
    it('barks', function() {
      dog = new Dog();
      assert.equal(dog.bark(), 'Bark')
    });
  });
});
