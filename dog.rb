# frozen_string_literal: true

# Basic Dog class
class Dog
  def bark
    'Woof'
  end

  def jump
    'Jump'
  end

  def eat
    'Eat'
  end

  def run
    'Run'
  end
end
